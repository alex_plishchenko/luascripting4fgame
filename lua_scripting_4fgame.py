# -*- coding: utf-8 -*-
import imp, os, re, shutil, sys, subprocess
import sublime_plugin, sublime
import urllib
import json

from .lua_scripting_4FGameClass import ProjectScript

try:
    sys.modules['lua_scripting_4FGameClass']
except KeyError:
    dirname, filename = os.path.split(os.path.abspath(__file__))
    path = dirname
    name = 'lua_scripting_4FGameClass'

    fp, pathname, description = imp.find_module(path +'/'+ name)

    try:
        imp.load_module(name, fp, pathname, description)
    finally:
        if fp:
            fp.close()
    pass

# ----------------------------------------------------------------------------------------
MARKER_INSERT_CUTSCENE     = "---------------------------CUTSCENES--------------------------------------------"
MARKER_INSERT_COMICS       = "---------------------------COMICS-----------------------------------------------"
MARKER_ZOOM_FUN_DEFINITION = "---------------------------ZOOM_FUN_DEFINITION----------------------------------"

# ----------------------------------------------------------------------------------------
""" General purpose trigger area description """
stub_object_description = "\tanim = CreateAnimation('Data/Textures/General/area_square.png',0,0,64,64,1,0)\n"\
                          "\tanim:SetHotSpot(32,32)\n"\
                          "\tanim:SetBlendMode(2)\n"\
                          "\tanim:SetColor('0x01FFFFFF')\n"\
                          "\tscreen:CreateObject(\'${0}\', anim, 512, 384, 100,0,3,3)\n"\
                          "\t\n"

strings_file_names = \
{
    'eng' : 'Data\Strings.txt',
}

def insert_object_description(script_path, description):
    script_file = open(script_path, 'r+')
    try:
        script_lines = script_file.readlines()
        if description not in script_lines:        
            script_file.seek(0)

            begic_index = script_lines.index('-- CLEAR_ON_SAVE_BEGIN\n')
            end_index = script_lines.index('-- CLEAR_ON_SAVE_END\n')
            
            if begic_index > end_index:
                end_index = script_lines.index('-- CLEAR_ON_SAVE_END\n', begic_index)

            script_lines.insert(end_index - 1, description)

            for line in script_lines:
                script_file.write(line)
    except Exception:
        print(sys.exc_info()[0])
        print(sys.exc_info()[1])
        pass
    finally:
        script_file.close()

# ===============================================================================================================
class ProjectScripting:
    def __init__(self):
        pass
        
    def creatItemCollection(self, tool_name, tool_number, marker_insert):
        table_name = 'collection_table'
        temp_inv = '00_temp_inv.png'

        item_collection_table = []
        item_collection_table.append(tool_name + 's')

        shutil.copyfile(os.path.join(self.project_paths.tools_path, temp_inv), os.path.join(self.project_paths.tools_path, tool_name + 's' + '.png'))

        for x in range(1, int(tool_number) + 1):
            item =  '{0}_{1}'.format(tool_name, x)
            item_collection_table.append(item)
            shutil.copyfile(os.path.join(self.project_paths.tools_path, temp_inv), os.path.join(self.project_paths.tools_path, item + '_inv.png'))

        item_str_table = (table_name + '[\'' + item_collection_table[0] + '\']').ljust(44, ' ') + '= {'

        for x in range(1, len(item_collection_table) - 1):
            item_str_table +=  '\'' + item_collection_table[x] + '\', '
        item_str_table += '\'' + item_collection_table[len(item_collection_table) - 1] + '\'}'

        template_file = open(os.path.join(self.project_paths.project_root_path, 'Data/Scripts/Local/Items/ItemsLocal.lua'), 'r+')
        lines = template_file.readlines()
        replaced_lines = [str.replace(x, marker_insert, item_str_table + '\n\t\t\t\t' + marker_insert +'') for x in lines]

        template_file.seek(0)        
        for line in replaced_lines:
            template_file.write(line)
        template_file.close()

        return item_str_table

    def creatHo(self, level_name):
        level_name = level_name[0].upper() + level_name[1:]
        root_level = self.project_paths.scene_name
        level_number = get_scene_number(root_level)
        level_index = str(int(level_number)) + 'ho'
        
        level_cmd  = 'RegisterLevel({0},      ''l_{1}_{2}'',                  ho_p..''{1}_{2}/{1}_{2}.lua'',                ho, l_''{3}'')'.format(level_index, level_number, level_name, root_level)
        link = "LinkScene('area_ho_{0}', 'CursorGlass', 'l_{1}_{2}', nil, nil, true)".format(level_name, level_number, level_name)        

        folder_name = level_number + '_' + level_name
        textures_path = os.path.join(self.project_paths.project_root_path, Path.ho_t + folder_name)
        script_path = os.path.join(self.project_paths.project_root_path, Path.ho_s + folder_name)

        if os.path.exists(script_path):
            print("Error create script_path")
            return None

        os.makedirs(script_path)
        templates_scripts_path = os.path.join(self.project_paths.templates_path, 'Scripts')

        template_file_path = os.path.join(templates_scripts_path, 'Scene_HO.lua')
        template_file = open(template_file_path, 'r')

        lines = template_file.readlines()
        lines.remove('DefineTemplate(\'__FILE_NAME__\')\n')
        replaced_lines = [str.replace(x, '__FILE_NAME__', folder_name) for x in lines]
        template_file.close()

        script_file_path = os.path.join(script_path, folder_name + '.lua')
        script_file = open(script_file_path, 'w')
        for line in replaced_lines:
            script_file.write(line)
        script_file.close()
        pass

        if os.path.exists(textures_path):
            print("Error create textures_path")
            return None

        os.makedirs(textures_path)
        templates_path = os.path.join(self.project_paths.templates_path, 'Textures')
        shutil.copyfile(os.path.join(templates_path, 'Background.jpg'), os.path.join(textures_path, 'Background.jpg'))

        sys.stdout.write( level_cmd +  '\n' + link)
        sys.stdout.write( textures_path +  '\n')
        sys.stdout.write('\n')
        sys.stdout.flush()
        
        return link


# ----------------------------------------------------------------------------------------
def Settings():
    return sublime.load_settings("LuaScript4FGame.sublime-settings")

project_script = ProjectScript()
settings = Settings().get("marker_insert_string",[])
if settings:
    project_script.getSettings(settings)
else:
    project_script.settings['bbt']   = "_BBT"
    project_script.settings['tools'] = "_TOOLS"

# =========================================================================================
#                                     sublime text commands
# =========================================================================================
class PsGameSetMarkerForZoom(sublime_plugin.TextCommand):
    def run(self, edit):
        regions = [s for s in self.view.sel() if not s.empty()]
        if regions:
            ProjectScript.marker_insert_for_zoom = self.view.substr(regions[0])
            print(ProjectScript.marker_insert_for_zoom)
        else:
            ProjectScript.marker_insert_for_zoom = ''
            print('no marker')

# ===================    new block ======================================================================
class PsGameOpenTextureFolderCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        if len(self.view.file_name()) > 0:
            path = project_script.dirOpenTexture(self.view.file_name())
            if path:
                sublime.status_message(path)
            else:
                sublime.status_message("Error :(")

class PsGameInsertMarkerCutsceneCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.insert(edit, 0, MARKER_INSERT_CUTSCENE + '\n')
        pass

# Insert marker Comics
class PsGameInsertMarkerComicsCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.view.insert(edit, 0, MARKER_INSERT_COMICS + '\n')
        pass

class PsGameCreateComicsAndFunCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        regions = [s for s in self.view.sel() if not s.empty()]
        if not regions: 
                return
        if len(self.view.file_name()) > 0:
            project_paths = project_script.projectDir(self.view.file_name())
            if not project_paths:
                return

        comics_inf   = self.view.substr(regions[0]).split(' ')
        comics_name  = comics_inf[0]

        if comics_inf[1]:
            i_frame = int(comics_inf[1])
            pass

        if comics_name[0].islower():
            comics_name = comics_name[0].upper() + comics_name[1:]

        find_region = self.view.find(MARKER_INSERT_COMICS, 0)
        comics_name_full = '{0}_{1}_Comics'.format(project_script.scene_number, comics_name)
        
        if not find_region:
            sublime.status_message("Error :)")
            print('not found marker:' + MARKER_INSERT_COMICS)
            return
        else:
            comics_fun_name   = 'Start_{0}_Comics'.format(comics_name)
            comics_frame_name =  'M_{0}_FRAME_1'.format(comics_name_full.upper())
            template_fun = \
            'function l_{0}:{4}()\n'\
            '\tlocal comics = Comics(\'{1}\', CreateComicsFrame(\'Default\', \'{3}\'))\n'\
            '\n'\
            '\tlocal function end_monolog(save)\n'\
            '\t\t--\n'\
            '\tend\n'\
            '\n'\
            '\tif IsComicsShown(\'{1}\') then\n'\
            '\t\tend_monolog(save)\n'\
            '\t\treturn\n'\
            '\tend\n'\
            '\n'\
            '\tcomics.on_frame_show = function()\n'\
            '\t\tif comics.frameIndex == 1 then\n'\
            '\t\t\t--\n'\
            '\t\tend\n'\
            '\tend\n'\
            '\n'\
            '\tcomics.on_finish = function()\n'\
            '\t\tself.zoom_comics_{2}:UnBlock()\n'\
            '\t\tself.zoom_comics_{2}:CloseAndDisable()\n'\
            '\t\tself:SetBlockerArea(false)\n'\
            '\t\tend_monolog(false)\n'\
            '\tend\n'\
            '\n'\
            '\tself.zoom_comics_{2}:Open(false, 0, true)\n'\
            '\tself.zoom_comics_{2}:Block()\n'\
            '\tself:SetBlockerArea(true)\n'\
            '\tcomics:Start()\n'\
            'end\n'\
            ''.format(project_paths.scene_name, comics_name_full, comics_name, comics_frame_name, comics_fun_name)

            project_script.insertString(self.view.file_name(), comics_frame_name)

            self.view.insert(edit, find_region.end() + 1, template_fun)
            self.view.run_command('insert_snippet', {'contents': 'self.zoom_comics_{0}  = self:createZoom(\'Comics_{0}\')\n'.format(comics_name)})
            self.view.run_command('insert_snippet', {'contents': 'self:{0}()'.format(comics_fun_name)})
            self.view.run_command('save')
            self.view.run_command('revert')

            #Create Zoom Dialog
            zoom_name = comics_name
            self.create_zoom_zone_command(project_paths, "Comics_"+ zoom_name, self.view.file_name())
            self.view.run_command('revert')
            # self.view.run_command('save')

    def create_zoom_zone_command(self, project_paths, zoom_name, script_file_name):
        self.create_zoom_folder(project_script.project_paths.textures_path, zoom_name)
        self.create_zoom_script(project_script.project_paths, zoom_name)

        zoom_trigger_name = stub_object_description.replace('${0}', 'area_zoom_' + zoom_name)
        insert_object_description(os.path.join(project_paths.scripts_path, script_file_name), zoom_trigger_name)
        pass

    def create_zoom_folder(self, parent_folder_path, zoom_name):
        zoom_folder_name = os.path.join(parent_folder_path, 'Zoom_' + zoom_name)
        if not os.path.exists(zoom_folder_name):
            os.makedirs(zoom_folder_name)

    def create_zoom_script(self, project_paths, zoom_name):
        zoom_template_file_path = os.path.join(project_paths.project_root_path, 'ZoomZoneTemplate.lua')
        zoom_template_file = open(zoom_template_file_path, 'r')

        lines = zoom_template_file.readlines()
        result_zoom_name = project_paths.scene_name + '_Zoom_' + zoom_name
        replaced_lines = [str.replace(x, 'INSERT_NAME',  'l_'+ result_zoom_name) for x in lines]

        result_script_file_path = os.path.join(project_paths.scripts_path, result_zoom_name + '.lua')
        result_script_file = open(result_script_file_path, 'w')
        for line in replaced_lines:
            result_script_file.write(line)
        result_script_file.close()
        zoom_template_file.close()
        pass

class PsGameCreateCutsceneAndFunCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        regions = [s for s in self.view.sel() if not s.empty()]
        if not regions: 
                return
        if len(self.view.file_name()) > 0:
            project_paths = project_script.projectDir(self.view.file_name())
            if not project_paths:
                return
        cutscene_name  = self.view.substr(regions[0])
        if cutscene_name[0].islower():
            cutscene_name = cutscene_name[0].upper() + cutscene_name[1:]

        find_region = self.view.find(MARKER_INSERT_CUTSCENE, 0)
        cutscene_file_name = '{0}_{1}'.format(project_script.scene_number, cutscene_name)
        if not find_region:
            sublime.status_message("Error :)")
            print('not found marker:' + MARKER_INSERT_CUTSCENE)
            return
        else:
            cs_fun_name = 'playCutscene_{0}'.format(cutscene_name)
            template_fun = \
            'function l_{0}:{1}()\n'\
            '\tlocal function finalize(save)\n'\
            '\t\tif not save then\n'\
            '\t\t\t--\n'\
            '\t\telse\n'\
            '\t\t\t--\n'\
            '\t\tend\n'\
            '\tend\n\n'\
            '\tlocal cutscene, shown = self:CreateCutscene(\'{2}\')\n'\
            '\tif not shown then\n'\
            '\t\tcutscene:SetFadeSettings(false, true, true)\n'\
            '\t\tcutscene:SetCallback(function()\n'\
            '\t\t                            finalize(false)\n'\
            '\t                       end)\n'\
            '\t\tcutscene:SetDuration(2)\n'\
            '\t\tcutscene:Play()\n'\
            '\telse\n'\
            '\t\tfinalize(true)\n'\
            '\tend\n'\
            'end\n'\
            ''.format(project_paths.scene_name, cs_fun_name, cutscene_file_name)

            self.view.insert(edit, find_region.end() + 1, template_fun)
            self.view.run_command('insert_snippet', {'contents': 'self:{0}()'.format(cs_fun_name)})

            placeholder_path = os.path.join(project_paths.project_root_path, "Movies/CS_Placeholder.ogv")
            movies_folder_name = os.path.join(project_paths.project_root_path, "Movies/{0}/".format(project_paths.scene_name))
            print(movies_folder_name)
            if not os.path.exists(movies_folder_name):
                print(movies_folder_name)
                os.makedirs(movies_folder_name)

            target_path = os.path.join(project_paths.project_root_path, "Movies/{0}/{1}.ogv".format(project_paths.scene_name, cutscene_file_name))

            shutil.copyfile(placeholder_path, target_path)

        self.view.run_command('save')
        self.view.run_command('revert')

class PsGameCreateItem(sublime_plugin.TextCommand):
    def run(self, edit):
        regions = [s for s in self.view.sel() if not s.empty()]
        if regions:
            item_name = self.view.substr(regions[0])
            project_paths = project_script.projectDir(self.view.file_name())
            if project_paths:
                project_script.creatItem(item_name, self.view.file_name())

class PsGameInsertStringCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        regions = [s for s in self.view.sel() if not s.empty()]
        if regions:
            string = self.view.substr(regions[0])
            project_script.insertString(self.view.file_name(), string)

class PsGameInsertAreaCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        regions = [s for s in self.view.sel() if not s.empty()]
        if regions:
            for x in range(0, len(regions)):
                print(self.view.substr(regions[x]))                
                pass
            area_name = self.view.substr(regions[0])
            # project_paths = ProjectScript.projectDir(self.view.file_name())
            # trigger_script_file_name = self.view.file_name()
            
            # zoom_name = ProjectScript.marker_insert_for_zoom
            # if len(zoom_name) != 0:
            #     trigger_script_file_name = get_zoom_file_name(project_paths, zoom_name)
            
            # if area_name.find('area_', 0, 5) == -1:
            #     area_name = 'area_'+ area_name

            # trigger_name = stub_object_description.replace('${0}', area_name)

            # insert_object_description(trigger_script_file_name, trigger_name)

            # self.view.run_command('revert')
# ===================   end new block ======================================================================


class PsGameCreateItemCollectionSe(sublime_plugin.TextCommand):
    def run(self, edit):
        if len(self.view.file_name()) > 0:
            project_paths = ProjectScript.projectDir(self.view.file_name())
            if project_paths:
                regions = [s for s in self.view.sel() if not s.empty()]
                if regions:
                    tool_name   = self.view.substr(regions[0]).split(' ')
                    if tool_name[0] and tool_name[1]:
                        if int(tool_name[1]) > 1:
                            item_str_table = ProjectScript.creatItemCollection_SE(tool_name[0], tool_name[1])
                            if len(item_str_table):
                                sublime.set_clipboard(item_str_table)
                                sublime.status_message("Create: " + tool_name[0])
                            else:
                                sublime.status_message("Error :)" + tool_name[0])

class PsGameCreateItemCollectionCe(sublime_plugin.TextCommand):
    def run(self, edit):
        if len(self.view.file_name()) > 0:
            project_paths = ProjectScript.projectDir(self.view.file_name())
            if project_paths:
                regions = [s for s in self.view.sel() if not s.empty()]
                if regions:
                    tool_name   = self.view.substr(regions[0]).split(' ')
                    if tool_name[0] and tool_name[1]:
                        if int(tool_name[1]) > 1:
                            item_str_table = ProjectScript.creatItemCollection_CE(tool_name[0], tool_name[1])
                            if len(item_str_table):
                                sublime.set_clipboard(item_str_table)
                                sublime.status_message("Create:" + tool_name)
                            else:
                                sublime.error_message("Error :)")

class CreateLevelCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        regions = [s for s in self.view.sel() if not s.empty()]
        if regions:
            self.view.run_command('save')

            level_name  = self.view.substr(regions[0])
            level_index = self.view.substr(regions[0])
            level_cmd  = 'RegisterLevel(\'$Index\',      \'l_$Index_$Name\',                  sc_p..\'$Index_$Name/$Index_$Name.lua\',                     qt)'

            sys.stdout.write(level_cmd +  '\n')
            sys.stdout.flush()

            sublime.status_message("Create Level")

class PsGameCreateHoCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        if len(self.view.file_name()) > 0:
            project_paths = 1 #ProjectScript.projectDir(self.view.file_name())
            if project_paths:
                regions = [s for s in self.view.sel() if not s.empty()]
                if regions:
                    level_name  = self.view.substr(regions[0])
                    str_link = ProjectScripting.creatHo(level_name)
                    if str_link:
                        self.view.run_command('insert_snippet', {'contents': str_link})
                        self.view.run_command('save')
                        sublime.status_message("Create HO: " + level_name)
                    else:
                        sublime.status_message("Error :)")

# Create Zoom Scene
class PsCreateZoomZoneSceneCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        regions = [s for s in self.view.sel() if not s.empty()]
        if not regions: 
                return
        if len(self.view.file_name()) > 0:
            project_paths = project_script.projectDir(self.view.file_name())
            if not project_paths:
                return
            zoom_name  = self.view.substr(regions[0])
            if zoom_name[0].islower():
                zoom_name = zoom_name[0].upper() + zoom_name[1:]

            snippet = self.get_zoom_zone_snippet(zoom_name)
            self.view.run_command('insert_snippet', {'contents': snippet})

            find_region = self.view.find(MARKER_ZOOM_FUN_DEFINITION, 0)
            # find_region = self.view.find("--zoom-\w+", 0)
            if not find_region:
                sublime.status_message("Error :)")
                print('not found marker:' + MARKER_ZOOM_FUN_DEFINITION)
                return
            else:
                zoom_maker = '--zoom-' + zoom_name.ljust(69, '-')
                self.view.insert(edit, find_region.end() + 1, 'function l_{0}:register_Zoom_{1}()\n\t{2}\nend\n\n'.format(project_paths.scene_name, zoom_name, zoom_maker))

            self.view.run_command('save')

            self.create_zoom_zone_command(project_paths, zoom_name, self.view.file_name())

            self.view.run_command('revert')

    def create_zoom_zone_command(self, project_paths, zoom_name, script_file_name):
        self.create_zoom_folder(project_script.project_paths.textures_path, zoom_name)
        self.create_zoom_script(project_script.project_paths, zoom_name)

        zoom_trigger_name = stub_object_description.replace('${0}', 'area_zoom_' + zoom_name)
        insert_object_description(os.path.join(project_paths.scripts_path, script_file_name), zoom_trigger_name)
        pass

    def create_zoom_folder(self, parent_folder_path, zoom_name):
        zoom_folder_name = os.path.join(parent_folder_path, 'Zoom_' + zoom_name)
        if not os.path.exists(zoom_folder_name):
            os.makedirs(zoom_folder_name)

    def create_zoom_script(self, project_paths, zoom_name):
        zoom_template_file_path = os.path.join(project_paths.project_root_path, 'ZoomZoneTemplate.lua')
        zoom_template_file = open(zoom_template_file_path, 'r')

        lines = zoom_template_file.readlines()
        result_zoom_name = project_paths.scene_name + '_Zoom_' + zoom_name
        replaced_lines = [str.replace(x, 'INSERT_NAME',  'l_'+ result_zoom_name) for x in lines]

        result_script_file_path = os.path.join(project_paths.scripts_path, result_zoom_name + '.lua')
        result_script_file = open(result_script_file_path, 'w')
        for line in replaced_lines:
            result_script_file.write(line)
        result_script_file.close()
        zoom_template_file.close()
        pass

    def get_zoom_zone_snippet(self, zoom_name):
        marker_zoom = '' #'--zoom-' + zoom_name.ljust(70, '-')
        crate_zomm = 'self.zoom_{0} = self:createZoom(\'{1}\')\n'.format(zoom_name, zoom_name)
        fun_name = 'self:register_Zoom_{0}()\n'.format(zoom_name)
        marker_zoom += crate_zomm + fun_name
        return marker_zoom