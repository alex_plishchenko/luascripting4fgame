class 'l___FILE_NAME__' (LevelLocal)

function l___FILE_NAME__:__init(name)
	LevelLocal.__init(self, name)

end
--------------------------------------------------------------------------------
-- CLEAR_ON_SAVE_BEGIN
-- Do not edit code listed below (generated by the editor on each save)
--------------------------------------------------------------------------------

DefineTemplate('__FILE_NAME__')

function l___FILE_NAME__:LoadData(screen)
	LevelLocal.LoadData(self, screen)
	local anim
	anim = CreateAnimation('Data/Textures/HiddenObjects/__FILE_NAME__/Background.jpg',0,0,1366,768,1,0)
	anim:SetHotSpot(683,384)
	anim:SetBlendMode(2)
	anim:SetColor('0xFFFFFFFF')
	screen:CreateObject('Background',anim,512,384,10,0,1,1)
end

-- CLEAR_ON_SAVE_END