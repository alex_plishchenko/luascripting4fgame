# -*- coding: utf-8 -*-

import os, re, shutil, sys, imp, subprocess

""" General purpose trigger area description """
stub_object_description = "\tanim = CreateAnimation('Data/Textures/General/area_square.png',0,0,64,64,1,0)\n"\
                          "\tanim:SetHotSpot(32,32)\n"\
                          "\tanim:SetBlendMode(2)\n"\
                          "\tanim:SetColor('0x01FFFFFF')\n"\
                          "\tscreen:CreateObject(\'${0}\', anim, 512, 384, 100,0,3,3)\n"\
                          "\t\n"

strings_file_names = \
{
    'eng' : 'Data\Strings.txt',
}
# ===============================================================================================================

def get_scene_number(scene_name):
    matches = re.findall("([0-9]+)_([A-Za-z_]*)", scene_name)
    scene_number = matches[0][0]
    return scene_number


def get_scene_name_by_number(strings_file_name, scene_number):
    strings_file = open(strings_file_name, 'r')
    try:
        strings_text = strings_file.read()
        scene_name = re.findall('(' + scene_number + '_[A-Za-z_]*)', strings_text)[0]
    except Exception:
        pass
    finally:
        strings_file.close()

    return scene_name

def insert_object_description(script_path, description):
    script_file = open(script_path, 'r+')
    print(script_path)
    try:
        script_lines = script_file.readlines()
        if description not in script_lines:        
            script_file.seek(0)

            begic_index = script_lines.index('-- CLEAR_ON_SAVE_BEGIN\n')
            end_index = script_lines.index('-- CLEAR_ON_SAVE_END\n')

            
            if begic_index > end_index:
                end_index = script_lines.index('-- CLEAR_ON_SAVE_END\n', begic_index)

            script_lines.insert(end_index - 1, description)

            for line in script_lines:
                script_file.write(line)
    except Exception:
        print(sys.exc_info()[0])
        print(sys.exc_info()[1])
        pass
    finally:
        script_file.close()

def get_zoom_file_name(project_paths, zoom_name):
    rel_zoom_file_name = project_paths.scene_name + '_Zoom_' + zoom_name + '.lua'
    abs_zoom_file_name = os.path.join(project_paths.scripts_path, rel_zoom_file_name)
    return abs_zoom_file_name
# ---------------------- Fun ---------------------------------

def get_strings_marker(scene_name, marker_type):
    return '; {0}{1}\n'.format(scene_name, marker_type)

def insert_string(strings_file_name, string_to_insert, after_marker):
    strings_file = open(strings_file_name, 'r+')
    try:
        strings_lines = strings_file.readlines()
        strings_text = "".join(strings_lines)

        if string_to_insert not in strings_text:
            strings_file.seek(0)
            marker_index = strings_lines.index(after_marker)
            strings_lines.insert(marker_index + 1, string_to_insert)
                # strings_lines.insert(index_insert, string_to_insert)

            for line in strings_lines:
                strings_file.write(line)

    except Exception:
        print(sys.exc_info()[0])
        print(sys.exc_info()[1])
    finally:
        strings_file.close()

def insert_stub_string(project_paths, string_key, after_marker):
    string_to_insert = '{0}= \"{1}\"\n'.format(string_key.ljust(32, ' '), string_key)

    strings_file_name = os.path.join(project_paths.project_root_path, strings_file_names['eng'])
    marker_name = get_strings_marker(project_paths.scene_name, after_marker)

    insert_string(strings_file_name, string_to_insert, marker_name)
# -------------------------------------------------------

class Paths:
    def __init__(self):
        self.sc_s     = 'Data/Scripts/Levels/'
        self.mg_s     = 'Data/Scripts/Puzzles/'
        self.ho_s     = 'Data/Scripts/HiddenObjects/'
        self.iz_s     = 'Data/Scripts/ItemsZoom/'
        self.sc_t     = 'Data/Textures/Levels/'
        self.mg_t     = 'Data/Textures/Puzzles/'
        self.ho_t     = 'Data/Textures/HiddenObjects/'
        self.items_t  = 'Data/Textures/Items'
        self.iz_t     = 'Data/Textures/ItemsZoom/'
        self.utility  = 'E:/Tools'

# -------------------------------------------------------

class c_project_paths():
    def __init__(self):
        self.project_root_path = ''
        self.textures_path = ''
        self.scripts_path = ''
        self.templates_path = ''
        self.scene_name = ''

class ProjectScript(object):
    def __init__(self):
        self.path = Paths()
        self.settings = dict.fromkeys(['bbt', 'tools'])

        self.marker_insert_for_zoom = ""
        self.scene_number = ''
        pass

    def getSettings(self, marker_string):
        for v in marker_string:
            self.settings['bbt']   = v["marker_bbt"]
            self.settings['tools'] = v["marker_tools"]

    def projectDir(self, filename):
        current_filename = filename.replace('\\', '/')
        matches = re.findall("(.*)" + self.path.sc_s + "(.*)/", current_filename)
        if not matches:
            matches = re.findall("(.*)" + self.path.ho_s + "(.*)/", current_filename)
            if not matches:
                matches = re.findall("(.*)" + self.path.mg_s + "(.*)/", current_filename)
                if not matches:
                    matches = re.findall("(.*)" + self.path.iz_s + "(.*)", current_filename)
                    if not matches:
                        return None

        project_root_path = matches[0][0]
        scene_name = matches[0][1]
        self.scene_number = get_scene_number(scene_name)

        if current_filename.find("Levels") != -1:
            textures_path = os.path.join(project_root_path, self.path.sc_t + scene_name)
        elif current_filename.find("Puzzles") != -1:
            textures_path = os.path.join(project_root_path, self.path.mg_t + scene_name)
        elif current_filename.find("HiddenObjects") != -1:
            textures_path = os.path.join(project_root_path, self.path.ho_t + scene_name)
        elif current_filename.find("ItemsZoom") != -1:
            textures_path = os.path.join(project_root_path, self.path.iz_t + re.split('.lua', scene_name)[0])

        scripts_path = os.path.join(project_root_path, self.path.sc_s + scene_name)

        templates_path = os.path.dirname(os.path.realpath(__file__)) + '/Templates'
        tools_path = os.path.join(project_root_path, self.path.items_t)

        self.project_paths = c_project_paths()
        self.project_paths.project_root_path = project_root_path
        self.project_paths.scene_name = scene_name
        self.project_paths.textures_path = textures_path
        self.project_paths.scripts_path = scripts_path
        self.project_paths.templates_path = templates_path
        self.project_paths.tools_path = tools_path

        return self.project_paths

    def dirOpenTexture(self, filename):
        project_paths = self.projectDir(filename)
        if project_paths:
            textures_path = project_paths.textures_path.replace('/', '\\')
            subprocess.call("explorer " + textures_path, shell=True)
            return textures_path
        else:
            return None

    def insertString(self, filename, string_to_insert):
        path = self.projectDir(filename)
        if path:
            insert_stub_string(path, string_to_insert, self.settings['bbt'])

    def creatItem(self, tool_name, filename):
        scene_number = get_scene_number(self.project_paths.scene_name)
        tool_name = tool_name.lower()
        tool_name_full = scene_number + '_' + tool_name
        item_file_name =  tool_name_full + '_inv.png'
        
        file_name_full = os.path.join(self.project_paths.tools_path, item_file_name)

        tool_DrawItems = os.path.join(self.path.utility, 'cmdDrawItems.exe')
        if os.path.exists(tool_DrawItems):
            str_cmd = tool_DrawItems + " " +  file_name_full + " " +  tool_name
            subprocess.check_call(str_cmd, shell=True)
        else:
            shutil.copyfile(os.path.join(self.project_paths.tools_path, '00_temp_inv.png'), file_name_full)

        item_string_to_insert = 'ITEM_NAME_{0}_{1}'.format(scene_number, tool_name.upper())

        insert_stub_string(self.project_paths, item_string_to_insert, self.settings['tools'])
        
        # ------------------------------
        string_to_insert = '{0}= \'{1}\'\n'.format(('local tool_' + tool_name).ljust(28, ' '), tool_name_full )

        strings_file = open(filename, 'r+')
        try:
            strings_lines = strings_file.readlines()
            strings_text = "".join(strings_lines)
            
            if string_to_insert not in strings_text:
                strings_file.seek(0)
                strings_lines.insert(0, string_to_insert)

                for line in strings_lines:
                    strings_file.write(line)

        except Exception:
            print(sys.exc_info()[0])
            print(sys.exc_info()[1])
        finally:
            strings_file.close()